package ru.t1.chubarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

public interface IConnectionProvider {

    @NotNull
    String getHost();

    @NotNull
    String getPort();

}
