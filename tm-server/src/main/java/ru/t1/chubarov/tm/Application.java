package ru.t1.chubarov.tm;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.component.Bootstrap;
import ru.t1.chubarov.tm.exception.AbstractException;

import java.io.IOException;

public final class Application {

    public static void main(@NotNull final String[] args) throws AbstractException, IOException {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}
