package ru.t1.chubarov.tm.api.service;

public interface IDomainService {

    String getFileBackup();

    void loadDataBackup();

    void saveDataBackup();

    void loadDataBase64();

    void saveDataBase64();

    void loadDataBinary();

    void saveDataBinary();

    void loadDataJsonFasterXml();

    void loadDataJsonJaxB();

    void saveDataJsonFasterXml();

    void saveDataJsonJaxB();

    void loadDataXmlFasterXml();

    void loadDataXmlJaxB();

    void saveDataXmlFasterXml();

    void saveDataXmlJaxB();

    void loadDataYamlFasterXml();

    void saveDataYamlFasterXml();

}
