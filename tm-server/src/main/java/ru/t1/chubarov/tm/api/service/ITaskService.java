package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnerService<Task> {

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description) throws AbstractException;

    @Nullable
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId) throws AbstractException;

    @NotNull
    Task updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description) throws AbstractException;

    @NotNull
    Task updateByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String name, @NotNull String description) throws AbstractException;

    @Nullable
    Task changeTaskStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status) throws AbstractException;

    @NotNull
    Task changeTaskStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) throws AbstractException;

}
