package ru.t1.chubarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.IRepository;
import ru.t1.chubarov.tm.api.service.IService;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.field.IdEmptyException;
import ru.t1.chubarov.tm.exception.field.IndexIncorrectException;
import ru.t1.chubarov.tm.model.AbstractModel;
import ru.t1.chubarov.tm.model.User;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @Nullable
    protected final R repository;

    protected AbstractService(@Nullable final R repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public M add(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @Override
    @NotNull
    public Collection<M> add(@NotNull final Collection<M> models) {
        return repository.add(models);
    }

    @Override
    @NotNull
    public Collection<M> set(@NotNull final Collection<M> models) {
        return repository.set(models);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @NotNull
    @Override
    public Collection<M> remove(@NotNull Collection<M> models) {
        return repository.remove(models);
    }

    @Override
    public void removeAll() {
        repository.removeAll();
    }

    @NotNull
    @Override
    public M remove(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        return repository.remove(model);
    }

    @NotNull
    @Override
    public M findOneById(@NotNull final String id) throws AbstractException {
        if (id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = repository.findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public M findOneByIndex(@NotNull final Integer index) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        @Nullable final M model = repository.findOneByIndex(index);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        if (id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeOneById(id);
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final Integer index) throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        return repository.removeOneByIndex(index);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

}
