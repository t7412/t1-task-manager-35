package ru.t1.chubarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.repository.IProjectRepository;
import ru.t1.chubarov.tm.api.repository.ITaskRepository;
import ru.t1.chubarov.tm.api.repository.IUserRepository;
import ru.t1.chubarov.tm.api.service.IProjectService;
import ru.t1.chubarov.tm.api.service.IPropertyService;
import ru.t1.chubarov.tm.api.service.IUserService;
import ru.t1.chubarov.tm.enumerated.ProjectSort;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.field.*;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.repository.ProjectRepository;
import ru.t1.chubarov.tm.repository.TaskRepository;
import ru.t1.chubarov.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

public class ProjectServiceTest {

    private static final int NUMBER_OF_ENTRIES = 3;

    @NotNull
    final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private IProjectService projectService;

    @NotNull
    private List<Project> projectList;

    @NotNull
    private String userUserId = "";

    @Before
    public void initTest() throws AbstractException {
        IPropertyService propertyService = new PropertyService();
        projectService = new ProjectService(projectRepository);
        projectList = new ArrayList<>();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        IUserService userService = new UserService(propertyService, userRepository, taskRepository, projectRepository);
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);
        @NotNull final User user = userService.create("user", "user", "user@emal.ru");
        userUserId = user.getId();
        @NotNull String userAdminId = admin.getId();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project Name " + i);
            project.setDescription("description test " + i);
            if (i <= 1) {
                project.setUserId(userAdminId);
            } else {
                project.setUserId(userUserId);
            }
            projectList.add(project);
        }
        projectService.set(projectList);
    }

    @Test
    public void testSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testCreate() {
        projectService.create(userUserId, "project with desc", "project description");
        projectService.create(userUserId, "project no desc", "");
        Assert.assertEquals(NUMBER_OF_ENTRIES + 2, projectService.getSize());
    }

    @Test
    @Category(UnitCategory.class)
    public void testCreateNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create("", "project_create", "project description"));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create(null, "project_create", "project description"));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(userUserId, "", "project description"));
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testFindOneByIndex() {
        @NotNull final String projectId = projectList.get(0).getId();
        @Nullable final String userId = projectList.get(0).getUserId();
        Assert.assertEquals(projectId, projectService.findOneByIndex(userId, 0).getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneByIndex("", 0).getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneByIndex(null, 0).getId());
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(userId, NUMBER_OF_ENTRIES + 1).getId());
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(userId, -1).getId());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testFindOneById() {
        @NotNull final String projectId = projectList.get(0).getId();
        @Nullable final String userId = projectList.get(0).getUserId();
        Assert.assertEquals(projectId, projectService.findOneById(userId, projectId).getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById("", projectId).getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById(null, projectId).getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void testFindAll() {
        @Nullable final List<Project> userProjectList = projectService.findAll(userUserId);
        Assert.assertEquals(2, userProjectList.size());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testFindAllSort() {
        @NotNull final ProjectSort sort = ProjectSort.toSort("BY_NAME");
        projectList.sort(sort.getComparator());
        @Nullable final List<Project> userProjectList = projectService.findAll(userUserId, sort.getComparator());
        Assert.assertEquals(2, userProjectList.size());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testRemoveOne() {
        projectService.removeOne(userUserId, projectList.get(1));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, projectService.getSize());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testRemoveOneById() {
        projectService.removeOneById(userUserId, projectList.get(1).getId());
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, projectService.getSize());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testRemoveOneByIndex() {
        projectService.removeOneByIndex(userUserId, 1);
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, projectService.getSize());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testChangeProjectStatusById() {
        @NotNull final String projectId = projectList.get(1).getId();
        Assert.assertEquals(Status.NOT_STARTED, projectService.findOneById(projectId).getStatus());
        projectService.changeProjectStatusById(userUserId, projectId, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findOneById(projectId).getStatus());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById("", projectId, Status.IN_PROGRESS));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById(null, projectId, Status.IN_PROGRESS));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(userUserId, "", Status.IN_PROGRESS));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(userUserId, null, Status.IN_PROGRESS));
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testChangeProjectStatusByIndex() {
        @NotNull final String projectId = projectList.get(1).getId();
        Assert.assertEquals(Status.NOT_STARTED, projectService.findOneById(projectId).getStatus());
        projectService.changeProjectStatusByIndex(userUserId, 1, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findOneById(projectId).getStatus());
        projectService.changeProjectStatusByIndex(userUserId, 1, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findOneById(projectId).getStatus());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusByIndex("", 1, Status.COMPLETED));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusByIndex(null, 1, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex(userUserId, -1, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex(userUserId, null, Status.COMPLETED));
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testUpdateById() {
        @NotNull final String projectId = projectList.get(1).getId();
        Assert.assertEquals("Project Name 2", projectService.findOneById(projectId).getName());
        Assert.assertEquals("description test 2", projectService.findOneById(projectId).getDescription());
        projectService.updateById(userUserId, projectId, "NewNameProject", "NewDescriptionProject");
        Assert.assertEquals("NewNameProject", projectService.findOneById(projectId).getName());
        Assert.assertEquals("NewDescriptionProject", projectService.findOneById(projectId).getDescription());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById("", projectId, "NewNameProject", "NewDescriptionProject"));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById(null, projectId, "NewNameProject", "NewDescriptionProject"));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(userUserId, "", "NewNameProject", "NewDescriptionProject"));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(userUserId, null, "NewNameProject", "NewDescriptionProject"));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(userUserId, projectId, "", "NewDescriptionProject"));

    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testUpdateByIndex() {
        @NotNull final String projectId = projectList.get(1).getId();
        Assert.assertEquals("Project Name 2", projectService.findOneById(projectId).getName());
        Assert.assertEquals("description test 2", projectService.findOneById(projectId).getDescription());
        projectService.updateByIndex(userUserId, 1, "NewNameProject", "NewDescriptionProject");
        Assert.assertEquals("NewNameProject", projectService.findOneById(projectId).getName());
        Assert.assertEquals("NewDescriptionProject", projectService.findOneById(projectId).getDescription());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateByIndex("", 1, "NewNameProject", "NewDescriptionProject"));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateByIndex(null, 1, "NewNameProject", "NewDescriptionProject"));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex(userUserId, null, "NewNameProject", "NewDescriptionProject"));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex(userUserId, -1, "NewNameProject", "NewDescriptionProject"));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateByIndex(userUserId, 1, "", "NewDescriptionProject"));
    }

}
