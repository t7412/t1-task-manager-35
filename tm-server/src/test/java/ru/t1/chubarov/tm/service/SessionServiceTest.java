package ru.t1.chubarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.repository.ISessionRepository;
import ru.t1.chubarov.tm.api.service.ISessionService;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.field.*;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.model.Session;
import ru.t1.chubarov.tm.repository.SessionRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class SessionServiceTest {

    private static final int NUMBER_OF_ENTRIES = 2;

    @NotNull
    final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private final String firstSessionId = UUID.randomUUID().toString();

    @NotNull
    private final String userUserId = UUID.randomUUID().toString();

    @NotNull
    private final String userAdminId = UUID.randomUUID().toString();

    @Before
    public void initTest() throws AbstractException {
        sessionService = new SessionService(sessionRepository);
        sessionList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            session.setDate(new Date());
            if (i <= 1) {
                session.setId(firstSessionId);
                session.setUserId(userAdminId);
            } else {
                session.setId(UUID.randomUUID().toString());
                session.setUserId(userUserId);
            }
            sessionList.add(session);
        }
        sessionService.set(sessionList);
    }

    @Test
    @Category(UnitCategory.class)
    public void testSize() throws AbstractException {
        Assert.assertEquals(1, sessionService.getSize(userAdminId));
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testAdd() {
        sessionService.add(userAdminId, new Session());
        sessionService.add(userAdminId, new Session());
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, sessionService.getSize(userAdminId));
    }

    @Test
    @Category(UnitCategory.class)
    public void testCreateNegative() {
        @Nullable final Session session = null;
        Assert.assertThrows(ModelNotFoundException.class, () -> sessionService.add(userAdminId, session));
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testExistsById() {
        Assert.assertTrue(sessionService.existsById(userAdminId,firstSessionId));
        Assert.assertFalse(sessionService.existsById(userUserId,firstSessionId));
        Assert.assertFalse(sessionService.existsById(userUserId,""));
        Assert.assertFalse(sessionService.existsById(userUserId,null));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.existsById("",firstSessionId));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.existsById(null,firstSessionId));
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testFindOneByIndex() {
        @Nullable final String userId = sessionList.get(0).getUserId();
        Assert.assertEquals(firstSessionId, sessionService.findOneByIndex(userAdminId, 0).getId());
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(userAdminId,NUMBER_OF_ENTRIES + 1).getId());
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(userAdminId,-1).getId());
        Assert.assertEquals(firstSessionId, sessionService.findOneByIndex(userId, 0).getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneByIndex("", 0).getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneByIndex(null, 0).getId());
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(userId, NUMBER_OF_ENTRIES + 1).getId());
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(userId, -1).getId());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testFindOneById() {
        @Nullable final String userId = sessionList.get(0).getUserId();
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.findOneById(userAdminId, "").getId());
        Assert.assertEquals(firstSessionId, sessionService.findOneById(userId, firstSessionId).getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneById("", firstSessionId).getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneById(null, firstSessionId).getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void testFindAll() {
        @Nullable final List<Session> userSessionList = sessionService.findAll(userUserId);
        Assert.assertEquals(1, userSessionList.size());
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveAll() throws AbstractException {
        sessionService.removeAll(userAdminId);
        Assert.assertEquals(1, sessionService.getSize());
        Assert.assertEquals(0, sessionService.getSize(userAdminId));
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testRemoveOne() {
        sessionService.removeOne(userUserId, sessionList.get(1));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionService.getSize());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testRemoveOneById() {
        sessionService.removeOneById(userUserId, sessionList.get(1).getId());
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionService.getSize());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void testRemoveOneByIndex() {
        sessionService.removeOneByIndex(userUserId, 1);
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionService.getSize());
    }

}
