package ru.t1.chubarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.repository.IProjectRepository;
import ru.t1.chubarov.tm.api.repository.ITaskRepository;
import ru.t1.chubarov.tm.api.repository.IUserRepository;
import ru.t1.chubarov.tm.api.service.IPropertyService;
import ru.t1.chubarov.tm.api.service.IUserService;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.field.EmailEmptyException;
import ru.t1.chubarov.tm.exception.field.ExistsLoginException;
import ru.t1.chubarov.tm.exception.field.LoginEmptyException;
import ru.t1.chubarov.tm.exception.field.PasswordEmptyException;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.repository.ProjectRepository;
import ru.t1.chubarov.tm.repository.TaskRepository;
import ru.t1.chubarov.tm.repository.UserRepository;
import ru.t1.chubarov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public class UserServiceTest {

    private static final int NUMBER_OF_ENTRIES = 3;

    @NotNull
    private IUserService userService;

    @NotNull
    private IPropertyService propertyService;

    @NotNull
    private List<User> userList;

    @Before
    public void initTest() throws AbstractException {
        propertyService = new PropertyService();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        userService = new UserService(propertyService, userRepository, taskRepository, projectRepository);
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);
        @NotNull final User user = userService.create("user", "user", "user@emal.ru");
        @NotNull final User cat = userService.create("cat", "caT", "car@catof.org");
        userList = new ArrayList<>();
        userList.add(admin);
        userList.add(user);
        cat.setLocked(true);
        userList.add(cat);
    }

    @Test
    @Category(UnitCategory.class)
    public void testSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, userService.getSize());
    }

    @Test
    @Category(UnitCategory.class)
    public void testAdd() throws AbstractException {
        @NotNull final User mouse = new User();
        mouse.setLogin("mouse");
        mouse.setPasswordHash(HashUtil.salt(propertyService, "password"));
        mouse.setEmail("mouse@test.org");
        userService.add(mouse);
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, userService.getSize());
    }

    @Test
    @Category(UnitCategory.class)
    public void testCreate() throws AbstractException {
        userService.create("dog", "dog", Role.ADMIN);
        userService.create("bear", "bear", "bear@emal.ru");
        userService.create("raccoon", "raccoon");
        Assert.assertEquals(NUMBER_OF_ENTRIES + 3, userService.getSize());
    }

    @Test
    @Category(UnitCategory.class)
    public void testCreateNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("","",""));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null,"dog","dog@mail.org"));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("dog","",""));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("dog",null,"dog@mail.org"));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.create("user","user","user@emal.ru"));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create("user","user",""));
    }

    @Test
    @Category(UnitCategory.class)
    public void testAddList() {
        userService.add(userList);
        Assert.assertEquals(NUMBER_OF_ENTRIES * 2, userService.getSize());
    }

    @Test
    @Category(UnitCategory.class)
    public void testSet() {
        userService.set(userList);
        Assert.assertEquals(NUMBER_OF_ENTRIES, userService.getSize());
    }

    @Test
    @Category(UnitCategory.class)
    public void testFindByLogin() throws AbstractException {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));
        Assert.assertNull(userService.findByLogin("admin").getEmail());
        Assert.assertEquals(Role.ADMIN, userService.findByLogin("admin").getRole());
    }

    @Test
    @Category(UnitCategory.class)
    public void testFindByEmail() throws AbstractException {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));
        Assert.assertEquals("admin", userService.findByLogin("admin").getLogin());
    }

    @Test
    @Category(UnitCategory.class)
    public void testClear() {
        userService.clear();
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemove() throws AbstractException {
        userService.remove(userList.get(1));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, userService.getSize());
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveList() {
        userService.remove(userList);
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveAll() {
        userService.removeAll();
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveOne() throws AbstractException {
        userService.removeOne(userList.get(1));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, userService.getSize());
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveByLogin() throws AbstractException {
        userService.removeByLogin("user");
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, userService.getSize());
        try {
            userService.findByLogin("user").getEmail();
        } catch (Exception e) {
            Assert.assertNull(e.getMessage());
        }
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveByEmail() throws AbstractException {
        userService.removeByEmail("user@emal.ru");
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, userService.getSize());
        try {
            userService.findByLogin("user").getEmail();
        } catch (Exception e) {
            Assert.assertNull(e.getMessage());
        }
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveNegative() throws AbstractException {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(""));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(""));
        Assert.assertNull(userService.removeOne(null));
    }

    @Test
    @Category(UnitCategory.class)
    public void testUnlock() throws AbstractException {
        Assert.assertTrue(userService.findByLogin("cat").getLocked());
        userService.unlockUserByLogin("cat");
        Assert.assertFalse(userService.findByLogin("cat").getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void testLock() throws AbstractException {
        Assert.assertFalse(userService.findByLogin("user").getLocked());
        userService.lockUserByLogin("user");
        Assert.assertTrue(userService.findByLogin("user").getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void testUpdateUser() throws AbstractException {
        userService.updateUser(userService.findByLogin("user").getId(), "Ivanov", "Peter", "Sidorovich");
        Assert.assertEquals("Ivanov", userService.findByLogin("user").getFirstName());
        Assert.assertEquals("Peter", userService.findByLogin("user").getLastName());
        Assert.assertEquals("Sidorovich", userService.findByLogin("user").getMiddleName());
    }

    @Test
    @Category(UnitCategory.class)
    public void testIsEmailExist() {
        Assert.assertFalse(userService.isEmailExist(null));
        Assert.assertFalse(userService.isEmailExist("user@1.s"));
        Assert.assertTrue(userService.isEmailExist("user@emal.ru"));
    }

    @Test
    @Category(UnitCategory.class)
    public void testIsLoginExist() {
        Assert.assertFalse(userService.isLoginExist(null));
        Assert.assertFalse(userService.isLoginExist("fail_login"));
        Assert.assertTrue(userService.isLoginExist("user"));
    }

    @Test
    @Category(UnitCategory.class)
    public void testExistsById() throws AbstractException {
        @NotNull final String userId = userService.findByLogin("user").getId();
        Assert.assertFalse(userService.existsById("fail_login"));
        Assert.assertTrue(userService.existsById(userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void testSetPassword() throws AbstractException {
        @NotNull final String userId = userService.findByLogin("user").getId();
        userService.setPassword(userId,"user_new_pas");
        Assert.assertEquals(HashUtil.salt(propertyService, "user_new_pas"), userService.findByLogin("user").getPasswordHash());
    }

}
